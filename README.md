# libasync-uv
A libasync wrapper to libuv

## Installation
This project uses a simple Makefile:

    sudo make install

## Disclaimer

This was mostly a toy project and the code definitely isn't optimal (for some reason it doesn't use futures, but weird combinations of mutexes).
