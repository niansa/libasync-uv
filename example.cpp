/*
 *  libasync-uv
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <uvpp.hpp>
#include <iostream>
#include <string>
#include <string_view>
using namespace std;

async::result<void> test(uvpp::loop_service &s, const uvpp::Addr &addr) {
    uvpp::tcp socket{s};
    co_await socket.connect(addr);
    co_await socket.send(std::string_view("Hello world!\n"));
    socket.recv_start();
    while (true) {
        // Read
        auto data = co_await socket.recv();
        std::clog << data->nread << std::endl;
        // Check for general error
        if (data->error()) {
            continue;
        }
        // Check for broken connection
        if (data->broken()) {
            break;
        }
        // Make string
        auto dataStr =
                std::string_view{data->data.get(),
                static_cast<std::string_view::size_type>(data->nread)};
        // Print it
        std::cout << dataStr << std::flush;
    }
}

int main() {
    using namespace uvpp;
    async::run_queue rq;
    async::queue_scope qs{&rq};
    loop_service service;

    async::detach(test(service, uvpp::make_ipv4("127.0.0.1", 1234)));
    async::run_forever(rq.run_token(), loop_service_wrapper{service});
}
