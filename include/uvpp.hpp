/*
 *  libasync-uv
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _UVPP
#define _UVPP
#include <async/basic.hpp>
#include <async/mutex.hpp>
#include <async/queue.hpp>
#include <async/result.hpp>
#include <frg/std_compat.hpp>
#include <cstring>
#include <memory>
#include <netinet/ip.h>
#include <optional>
#include <string_view>
#include <uv.h>

namespace uvpp {
class loop_service {
public:
    explicit loop_service(uv_loop_t *loop) : loop(loop) {}
    loop_service() : loop_service(uv_default_loop()) {}

private:
    struct loop_deleter {
        void operator()(uv_loop_t *loop) {
            assert(!uv_loop_close(loop));
            delete loop;
        }
    };
    std::unique_ptr<uv_loop_t, loop_deleter> loop;

    friend struct tcp;

public:
    void wait() { uv_run(loop.get(), UV_RUN_ONCE); }
};

class loop_service_wrapper {
public:
    loop_service &sv;
    void wait() { sv.wait(); }
};

namespace {
void alloc_buffer(uv_handle_t *handle, size_t size, uv_buf_t *buf) {
    buf->base = static_cast<char *>(operator new(size));
    buf->len = size;
}
using Addr = sockaddr;
static auto make_ipv4(std::string_view address, int port) {
    sockaddr_in fres;
    uv_ip4_addr(address.data(), port, &fres);
    return *reinterpret_cast<Addr *>(&fres);
}
static auto make_ipv6(std::string_view address, int port) {
    sockaddr_in6 fres;
    uv_ip6_addr(address.data(), port, &fres);
    return *reinterpret_cast<Addr *>(&fres);
}
template <class T, class baseT> void setDataMember(baseT *self, T& t) {
    t.data = static_cast<void *>(self);
}
} // namespace

class tcp {
public:
    // Basic stuff
    struct [[nodiscard]] received {
        std::unique_ptr<const char> data;
        size_t bufsize;
        ssize_t nread;

        bool error() { return data == nullptr || bufsize <= 0; }
        bool broken() { return nread < 0; }
    };

    explicit tcp(loop_service &service) : service(service) {
        uv_tcp_init(service.loop.get(), &uv_tcp);
        setDataMember(this, uv_tcp);
    }

    // Receive
    void recv_start() {
        if (recv_started) {
            return;
        }
        uv_read_start(reinterpret_cast<uv_stream_t *>(&uv_tcp), &alloc_buffer,
                      &on_read);
        recv_started = true;
    }

    void recv_stop() {
        if (!recv_started) {
            return;
        }
        uv_read_stop(reinterpret_cast<uv_stream_t *>(&uv_tcp));
        recv_started = false;
    }

    auto recv() { return recvQueue.async_get(); }

    // Send
    auto send(const char *data, size_t len) {
        uv_buf_t buffer[] = {{.base = const_cast<char *>(data), .len = len}};
        auto pWrite = new uv_write_t;
        setDataMember(this, *pWrite);
        uv_write(pWrite, reinterpret_cast<uv_stream_t *>(&uv_tcp), buffer, 1,
                 &on_write);

        return sendQueue.async_get();
    }

    template <class T> auto send(const T &data) {
        return send(data.data(), data.size());
    }

    // Keepalive
    void keepalive(bool enable, unsigned int delay) {
        uv_tcp_keepalive(&uv_tcp, enable, delay);
    }

    // Connect
    auto connect(const Addr &addr) {
        auto pConn = new uv_connect_t;
        setDataMember(this, *pConn);
        uv_tcp_connect(pConn, &uv_tcp, &addr, &on_connect);

        return connectQueue.async_get();
    }

    // Close
    auto close() {
        recv_stop();
        closeLock.try_lock();
        uv_close(reinterpret_cast<uv_handle_t *>(&uv_tcp), &on_close);

        return closeLock.async_lock();
    }
    ~tcp() { recv_stop(); }

private:
    // Basic stuff
    uv_tcp_t uv_tcp;
    bool recv_started = false;
    loop_service& service;

    // Async queues
    async::queue<received, frg::stl_allocator> recvQueue;
    async::queue<int, frg::stl_allocator> sendQueue;
    async::queue<int, frg::stl_allocator> connectQueue;
    async::mutex closeLock;

    // Callbacks
    static void on_read(uv_stream_t *handle, ssize_t nread, const uv_buf_t *buf) {
        received r{std::unique_ptr<const char>{buf->base}, buf->len, nread};
        auto self = static_cast<tcp *>(handle->data);
        self->recvQueue.emplace(std::move(r));
    }

    static void on_write(uv_write_t *handle, int status) {
        auto self = static_cast<tcp *>(handle->data);
        self->sendQueue.emplace(status);
        delete handle;
    }

    static void on_connect(uv_connect_t *handle, int status) {
        auto self = static_cast<tcp *>(handle->data);
        self->connectQueue.emplace(status);
        delete handle;
    }

    static void on_close(uv_handle_t *handle) {
        auto self = static_cast<tcp *>(handle->data);
        self->closeLock.unlock();
    }
};
} // namespace uvpp
#endif
